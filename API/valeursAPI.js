import axios from 'axios'

export function get_chart() {
    const url = 'http://10.2.0.4:3000/api/valeurs'
    return new Promise((resolve, reject) => {
        return axios.get(url).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        })
    })
}