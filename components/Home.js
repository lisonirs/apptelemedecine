import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {LineChart} from "react-native-chart-kit";
import { get_chart } from '../API/valeursAPI.js'

 class Linedchart extends React.Component {
  state = {
    datasource:[]
  };

  LineChart_Dynamic=()=>{
    if (this.state.datasource){
    if(this.state.datasource.length){

  return(
    <View>

      <View style={styles.containerTitle}>
        <Text style={styles.title}>Courbe EMG:</Text>
      </View>

      <View style={styles.containerChart}>
        <LineChart
          data={{
            labels: [
              (new Date()).getTime()
            ],
            datasets: [
              {
                data:  this.state.datasource.map(item=>{
                  return(
                    item.value 
                  )
                })
              }
            ]
          }}

        width={Dimensions.get("window").width}
        height={220}
        yAxisLabel="value"
        yAxisSuffix="k"
        yAxisInterval={1}
        chartConfig={{
          backgroundColor: "#e26a00",
          backgroundGradientFrom: "#fb8c00",
          backgroundGradientTo: "#ffa726",
          decimalPlaces: 2, 
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16
          },
          propsForDots: {
            r: "6",
            strokeWidth: "2",
            stroke: "#ffa726"
          }
        }}
        style={styles.chart}
      />
    </View>
  </View>
  )
  }

  }else {
    return(
      <View style={{justifyContent:"center",alignItems:'center',flex:1}}>
        <Text>no values found</Text>
      </View>
    )}

    }
  get_chart=()=>{
    this.get_chart()
  }

    componentDidMount=()=>{
        get_chart().then(res => {
            this.setState({ datasource:response })
        }).catch(err => {
            console.log(err)
        })
    }

    render() {
      return(
        <View>
          {this.LineChart_Dynamic()}
        </View>
      )
}}

const styles = StyleSheet.create({
  containerTitle: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: '#6495ed',
    textAlign: 'center',
    fontSize: 30,
    fontStyle: 'italic',
    letterSpacing: 2,
    marginTop: 50
  },
  containerChart: {
    flex: 2,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  chart: {
    marginVertical: 8,
    borderRadius: 16
  }
});

export default Linedchart;
