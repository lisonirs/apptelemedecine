import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Home from '../components/Home'

const TelemedecineNavigator = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
        }
    }
})

export default createAppContainer(TelemedecineNavigator)
